# yz858-ids721-mini8

This project is based on mini-project-6 to build a Rust CLI tool for users to query fruit price through terminal.

## Requirements
- [x] Tool functionality: rust command-line tool (30%)
- [x] Data ingestion/processing (30%)
- [x] Unit testing implementation (30%)
- [x] Documentation (10%)

## Steps
1. Create a new rust app
```
// --bin flag here creates a binary crate with main function which acts like an like
// rather than a library crate without main function and acts like a library/module to other files
cargo new NAME --bin
```

2. Run code to process arguments passed in by users
```
cargo run -- apple
cargo run -- pear
cargo run -- orange
```

3. Run test code to test functionality
```
cargo test
```


## Screenshots
1. Sample output of the tool
![Output](/screenshots/SampleOutput.png)

1. Test report
![Test](/screenshots/TestReport.png)