use lazy_static::lazy_static;
use std::collections::HashMap;
use std::env;

// Fruit price dictionary
lazy_static! {
    static ref FRUITS_INFO: HashMap<String, f64> = {
        let mut map = HashMap::new();
        map.insert("apple".to_string(), 3.0);
        map.insert("orange".to_string(), 2.0);
        map.insert("pear".to_string(), 1.5);
        map
    };
}

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        eprintln!("Usage: {} <input_string>", args[0]);
        std::process::exit(1);
    }

    let fruit = &args[1];

    println!("The price of {} is {}", fruit, get_price(fruit));
}

fn get_price(fruit: &str) -> f64 {
    FRUITS_INFO.get(fruit).copied().unwrap_or(0.0)
}


#[cfg(test)]
mod tests {
    use super::*;  // Import everything from the outer module

    #[test]
    fn test_get_price_apple() {
        // Test a fruit that exists in the dictionary
        assert_eq!(get_price("apple"), 3.0);
    }

    #[test]
    fn test_get_price_pear() {
        // Test a fruit that exists in the dictionary
        assert_eq!(get_price("pear"), 1.5);
    }

    #[test]
    fn test_get_price_orange() {
        // Test a fruit that exists in the dictionary
        assert_eq!(get_price("orange"), 2.0);
    }


    #[test]
    fn test_get_price_not_found() {
        // Test a fruit that does not exist in the dictionary
        assert_eq!(get_price("banana"), 0.0);
    }
}